package com.example.task2;

class Mylist {
    private String Title;
    private String Description;
    private int Image;

    public Mylist(
            String description,int imgid){
        this.Description=description;
        this.Image=imgid;
    }

    public Mylist(String title, String description, int image) {
        Title = title;
        Description = description;
        Image = image;
    }

    public String getTitle() {
        return Title;
    }

    public String getDescription() {
        return Description;
    }

    public int getImage() {
        return Image;
    }


    public void setTitle(String title) {
        Title = title;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setImage(int image) {
        Image = image;
    }
}

