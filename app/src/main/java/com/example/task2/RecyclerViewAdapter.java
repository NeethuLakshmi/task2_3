package com.example.task2;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    private Context cntext;
    private List<Mylist> data;

    public RecyclerViewAdapter(Context context, List<Mylist> data) {
        this.cntext = mContext;
        this.data = data

    }


    public static class MyViewHolder extends RecyclerViewAdapter.ViewHolder {
       TextView title,price;
        ImageView imageView;
        CardView  cardView;

        public MyViewHolder (View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.textID);
            price = (TextView) itemView.findViewById(R.id.textView);
            imageView = (ImageView) itemView.findViewById(R.id.imgID);
            cardView = (CardView) itemView.findViewById(R.id.CardViewID);


        }

    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        LayoutInflater layoutInflater = LayoutInflater.from(contex);
        view= layoutInflater.inflate(R.layout.caerdview_items,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter holder, int position) {
        holder.title.setText(data.get(position).getTitle());
        holder.price.setText(data.get(position).getPrice());
        holder.imageView.setImageResource(data.get(position).getImage());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               Intent intent;
                if (position == 0 ){
                    Toast.makeText(cntext,"Position 0",Toast.LENGTH_LONG).show();
                }
                else if (position == 1 ){
                    Toast.makeText(context,"Position 1",Toast.LENGTH_LONG).show();
                }
                else if (position == 2 ){

                }


            }
        });

    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
