package com.example.task2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {
    List<Mylist> itemsList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        itemsList = new ArrayList<>();
        itemsList.add(new Items("Carrot","Rs.50",R.drawable.ic_items));
        itemsList.add(new Items("Colgate","Rs.10",R.drawable.ic_items));
        itemsList.add(new Items("Corn","Rs.30",R.drawable.ic_items));
        itemsList.add(new Items("ToothBrush","Rs.20",R.drawable.ic_items));
        itemsList.add(new Items("Apple","Rs.50",R.drawable.ic_items));
        itemsList.add(new Items("Grapes","Rs.40",R.drawable.ic_items));
        itemsList.add(new Items("Watermelon","Rs.30",R.drawable.ic_items));

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerID);
        RecyclerViewAdapter myAdapter = new RecyclerViewAdapter(this, itemsList);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(myAdapter);


    }
}